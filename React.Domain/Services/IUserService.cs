﻿using MongoDB.Driver;
using Practice.Models.ConnectionString;
using React.Data.Entitys;
using React.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace React.Domain.Services
{
    public interface IUserService : IBaseService<User>
    {
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<User> CreateAsync(User entity)
        {
            await _userRepository.Create(entity);
            return entity;
        }

        public async Task DeleteByIdAsync(string id)
        {
            await _userRepository.Delete(id);
        }

        public async Task<User> GetByIdAsync(string id)
        {
            var user = await _userRepository.Get(id);
            return user;
        }

        public async Task<User> UpdateAsync(User entity)
        {
            await _userRepository.Update(entity);
            return entity;
        }
    }
}


