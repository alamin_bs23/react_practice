﻿using React.Data.Entitys;
using React.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace React.Domain.Services
{
    public interface IBookService : IBaseService<Book>
    {
       
    }

    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;

        public BookService(IBookRepository bookRepository)
        {
            this._bookRepository = bookRepository;
        }
        public async Task<Book> CreateAsync(Book entity)
        {
            await _bookRepository.Create(entity);
            return entity;
        }

        public async Task DeleteByIdAsync(string id)
        {
            await _bookRepository.Delete(id);
        }

        public async Task<Book> GetByIdAsync(string id)
        {
            var user = await _bookRepository.Get(id);
            return user;
        }

        public async Task<Book> UpdateAsync(Book entity)
        {
            await _bookRepository.Update(entity);
            return entity;
        }
    }
}
