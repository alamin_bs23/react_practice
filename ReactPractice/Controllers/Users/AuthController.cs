﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Practice.Models.Users;
using React.Data.Entitys;
using React.Domain.Services;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ReactPractice.Controllers.Users
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService UserService;
        private readonly ILogger logger;

        public AuthController(
            IUserService UserService,
            ILogger logger
            )
        {
            this.UserService = UserService;
            this.logger = logger;
        }


        [HttpPost, Route("signin")]
        public async Task<IActionResult> Signin(UserModel model, CancellationToken cancellationToken)
        {
            try
            {
                return Ok(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost, Route("User")]
        public async Task<IActionResult> Create(User User)
        {
            this.logger.Information("User is creating!");
            User = await UserService.CreateAsync(User);
            this.logger.Information("User is created!");

            return Ok(User);
        }

        [HttpPut, Route("update")]
        public async Task<IActionResult> Update(User user)
        {
            if(user.Password == "1234")
            {
                throw new ArgumentException($"user password so week {user.Password}");
            }
            user = await UserService.UpdateAsync(user);
            return Ok(user);
        }
    }
}
