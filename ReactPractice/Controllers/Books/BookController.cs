﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using React.Data.Entitys;
using React.Domain.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ReactPractice.Controllers.Books
{
    [Route("api/book")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService bookService;

        public BookController(IBookService bookService)
        {
            this.bookService = bookService;
        }

        [HttpPost, Route("create")]
        public async Task<IActionResult> Create(Book book, CancellationToken cancellationToken)
        {
            try
            {
                await this.bookService.CreateAsync(book);
                return Ok(book);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
