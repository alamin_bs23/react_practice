﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReactPractice.Controllers
{
    public abstract class ApiControllerBase : ControllerBase
    {
        public string userName => User.Identity.Name;
        public ApiControllerBase()
        {

        }
    }
}
