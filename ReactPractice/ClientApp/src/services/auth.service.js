﻿class AuthService {
    async post(url, data) {
        const rawResponse = await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        const content = rawResponse.json();

        return content;
    }

    async login(username, password) {
        var data = {
            username: username,
            password: password
        };
        return await this.post('api/auth/signin', data);
    }
}

export default new AuthService();