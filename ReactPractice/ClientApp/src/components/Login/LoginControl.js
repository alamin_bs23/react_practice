﻿import { Button } from 'bootstrap';
import React, { Component } from 'react';

export class LoginControl extends Component {
    static displayName = LoginControl.name;

    constructor(props) {
        super(props);
        this.handleLoginClick = this.handleLoginClick.bind(this);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
        this.state = { isLoggedIn: false };
    }

    handleLoginClick() {
        this.setState({ isLoggedIn: true });
    }

    handleLogoutClick() {
        this.setState({ isLoggedIn: false });
    }

    componentDidMount() {
       // this.populateWeatherData();
    }

    LoginButton(props) {
        return (
            <button onClick={props.onClick}>
                Login
            </button>
        );
    }

    LogoutButton(props) {
        return (
            <button onClick={props.onClick}>
                Logout
            </button>
        );
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn;
        let button;
        if (isLoggedIn) {
            button = <this.LogoutButton onClick={this.handleLogoutClick} />
        }
        else {
            button = <this.LoginButton onClick={this.handleLoginClick} />;
        }

        return (
            <div>
                {button}
            </div>
        );
    }
}

function UserGreeting(props) {
    return <h1>Welcome back!</h1>;
}

function GuestGreeting(props) {
    return <h1>Please sign up.</h1>;
}

function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GuestGreeting />;
}