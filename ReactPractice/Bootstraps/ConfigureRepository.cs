﻿using Microsoft.Extensions.DependencyInjection;
using React.Data.Entitys;
using React.Data.Repository;
using React.Domain.Services;

namespace ReactPractice.Bootstraps
{
    public static class ConfigureRepository
    {
        public static void RepositoryConfigure(this IServiceCollection services)
        {
            services.AddScoped<IMongoDbContext<User>, MongoDbContext<User>>();
            services.AddScoped<IMongoDbContext<Book>, MongoDbContext<Book>>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
        }
    }
}
