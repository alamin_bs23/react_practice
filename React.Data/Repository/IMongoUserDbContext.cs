﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Practice.Models.ConnectionString;
using React.Data.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace React.Data.Repository
{
    public interface IMongoDbContext<T> where T : BaseEntity
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }
     
    public class MongoDbContext<T> : IMongoDbContext<T> where T : BaseEntity
    {
        private IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }
        public IClientSessionHandle Session { get; set; }
        public MongoDbContext(IDatabaseSettings configuration)
        {
            _mongoClient = new MongoClient(configuration.ConnectionString);
            _db = _mongoClient.GetDatabase(configuration.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }
}
