﻿using Practice.Models.ConnectionString;
using React.Data.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace React.Data.Repository
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }

    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IMongoDbContext<User> context) : base(context)
        {
        }
    }
}
