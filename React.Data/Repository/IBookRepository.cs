﻿using React.Data.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace React.Data.Repository
{
    public interface IBookRepository : IBaseRepository<Book>
    {
    }

    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        public BookRepository(IMongoDbContext<Book> context) : base(context)
        {
        }
    }
}
