﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace React.Data.Entitys
{
    public class Book : BaseEntity
    {
        public string AuthorName { get; set; }
        public string BookName { get; set; }
        public string TotalPage { get; set; }
    }
}
